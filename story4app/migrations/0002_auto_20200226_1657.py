# Generated by Django 3.0.2 on 2020-02-26 09:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('story4app', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='friend',
            name='favourite_drink',
            field=models.CharField(max_length=25),
        ),
        migrations.AlterField(
            model_name='friend',
            name='favourite_food',
            field=models.CharField(max_length=255),
        ),
        migrations.AlterField(
            model_name='friend',
            name='hobby',
            field=models.CharField(max_length=255),
        ),
    ]
