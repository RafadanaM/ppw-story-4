from django.urls import include, path
from .views import index, friendAdd, friendView

urlpatterns = [
    path('', index, name = 'index'), 
    path('add/', friendAdd, name = 'friendadd'),
    path('friendview/',friendView, name = 'friendviews'  ),
    
]