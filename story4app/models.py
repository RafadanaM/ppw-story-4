from django.db import models
from django.forms import ModelForm
# Create your models here.

class BatchYear(models.Model):
    yearBatch = models.CharField(max_length=50)

    def __str__(self):
        return self.yearBatch


class Friend(models.Model):
    objects = models.Manager()
    full_name = models.CharField(max_length = 255, primary_key = True, unique = True) 
    batch_year = models.ForeignKey(BatchYear, on_delete=models.CASCADE, default='Other')
    hobby = models.CharField(max_length = 255)
    favourite_food = models.CharField(max_length = 255)
    favourite_drink = models.CharField(max_length = 25)

    def __str__(self):
        return self.full_name

