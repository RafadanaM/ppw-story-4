from django import forms
from .models import Friend

class FriendForm(forms.ModelForm):
    class Meta:
        model = Friend
        fields = ('full_name', 'batch_year', 'hobby', 'favourite_food', 'favourite_drink')
        widgets = {'full_name' : forms.TextInput(attrs={'class' : 'form-control', 'placeholder' : 'TEST'}),
                    'batch_year' : forms.Select(attrs ={'class' : 'form-control'}),
                    'hobby' : forms.TextInput(attrs ={'class' : 'form-control'}),
                    'favourite_food' : forms.TextInput(attrs ={'class' : 'form-control'}),
                    'favourite_drink' : forms.TextInput (attrs ={'class' : 'form-control'})}

