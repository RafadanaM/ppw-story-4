from django.shortcuts import render
from .models import Friend
from .forms import FriendForm
nama = 'Rafa'
# Create your views here.
def index(request):
    
    return render(request, 'index.html')  



def friendAdd(request):
    
    if request.method == 'POST':
        form = FriendForm(request.POST)
        if form.is_valid():
            form.save()

    form = FriendForm()
    response = {'form' : form}
    return render(request, 'friendAdd.html', response)

def friendView(request):

    friendList = Friend.objects.all()

    return render(request, 'friendView.html', {'friendList': friendList})